#!/usr/bin/env python3.4

from __future__ import print_function
import pymysql
import sys, getopt, traceback


def usage():
    s = """
usage: koquery.py -p password [-h] [-u mysqluser] [-s seconds] [-q query]
       [-o owner]

       Command Summary:
        -p password             mandatory input. password for the mysql server. 
        -u mysqluser            username for connection to the mysql server. default is root.
        -s seconds              kill threads equal or under this seconds threashold.  
        -q query                kill threads running this query. 
        -o owner                kill threads with querys executed bu owner.
        -h                      print this usage message
        
        e.g:
        python koquery.py -u dbadmin -p mypassword -s 5 -o user1 -q select
        
        if only default values are used or just "user" and "password" are entered, the 
        thread list will only be printed out and no one will get killed.
        """
        
    print(s)
    sys.exit(1)
    
def main():
    
    try:
        # getopts from command line
        opts, args = getopt.getopt(sys.argv[1:], "hu:p:s:q:o:", [])
        
        username = 'root'
        password = None
        seconds = 0
        info_query = None
        owner = None
        op_index = 0 # mark this 0 for listing, 1 for killing threads
        
        for o, a in opts:
            if o == "-h":
                usage()
                sys.exit()
            elif o == "-u":
                username = a
            elif o == "-p":
                password = a
            elif o == "-s":
                seconds = int(a)
            elif o == "-q":
                info_query = a
            elif o == "-o":
                owner = a
            else:
                assert False, "unhandled option"
                 
     
        # option validation     
        if password == None:
            print("database password must be supplied with the -p flag")
            usage()
            
        # create connection to mysql db
        conn = pymysql.connect(host='localhost', port=3306, user=username, password=password, db='mysql')
            
        # building sql query
        sql = "SELECT ID,USER,TIME,STATE,INFO FROM information_schema.processlist WHERE TRUE"
        if seconds > 0:
            op_index = 1
            sql = sql + " AND TIME >= " + str(seconds)
            
        if owner != None:
            op_index = 1
            sql = sql + " AND USER = '" + owner + "'"
            
        if info_query != None:
            op_index = 1
            sql = sql + " AND INFO LIKE '%" + info_query +"%'" 
              
        
        # run query
        cur = conn.cursor()
        cur.execute(sql)
        
        # sql kill threads query
        k_cur = conn.cursor()
        
        if op_index == 0:
            print("Listing all active mysql thread")
        else:
            print("Killing mode")
            
        # run query and process result
        count =  0
        for row in cur.fetchall():
            count=count + 1
            if op_index == 1:
                k_sql = "kill " + str(row[0])
                k_cur.execute(k_sql)
                print("ko - thread id: "  , row[0], " user: " , row[1] , " time: ", row[2], " state: ", row[3], " info: ", row[4])
            else:    
                print("Mysql thread- id: ", row[0], " user: " , row[1] , " time: ", row[2], " state: ", row[3], " info: ", row[4])
             
        action = "listed" if op_index == 0 else "killed"
        print("Number of treads " + action + ": " , count)
        
            
        cur.close()
        k_cur.close()
        conn.close()
            
    except getopt.GetoptError as err:
        print(str(err)) 
        usage()
        sys.exit(2)
    except KeyboardInterrupt:
        print("Shutdown requested...exiting")
    except Exception:
        traceback.print_exc(file=sys.stdout)   
    sys.exit(0)
    

if __name__ == "__main__":
    main()