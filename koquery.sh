#!/bin/bash
: '
OriB
MYSQL PUNY THREAD KILLER 
'
#----------------------------------------------------------------------
# default variable values
#----------------------------------------------------------------------
TRUE=1
FALSE=0
PROGNAME=$(basename $0)
# mysql user name
USER_NAME='root'
# mysql password
PASSWORD=""    
# query parameters: seconds, info and user
SECS=""
QUERY="" 
OWNER=""
# operational index. 0 for only thread listing. 1 for killing mode
OP_INDEX=0
# sql query, the basic part
SQL_QUERY="SELECT ID,USER,TIME,STATE,INFO FROM information_schema.processlist WHERE TRUE"
#----------------------------------------------------------------------
# functions
#----------------------------------------------------------------------

# function for usage printout
function usage(){

cat <<- __HERE
usage: koquery [-h] [-u mysqluser] [-p password] [-s seconds] [-q query]
	  [-o owner]

       Command Summary:
        -u mysqluser	        username for connection to the mysql server. default is root.
        -p password		password for the mysql server. default is none. 
        -s seconds		kill threads equal or under this seconds threashold.  
        -q query		kill threads running this query. 
        -o owner		kill threads with querys executed bu owner.
        -h			print this usage message
        
        e.g: 
        ./koquery -u dbadmin -p mypassword -s 5 -o user1 -q select
        
        if only default values are used or just "user" and "password" are entered, the 
        thread list will only be printed out and no one will get killed.
		
__HERE
	exit 1	
}


# function for exit due to fatal program error
# accepts 1 argument:  string containing descriptive error message
function error_exit(){
	echo "${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
	exit 1
}


# checks if passed argument is a decimal
# accepts 1 argument: argument under test
function is_decimal(){

	re='^[0-9]+$'
	if [[ $1 =~ $re ]] ; then
   		return $TRUE
	fi
	
	return $FALSE
	
}

# create the final sql query and calculates op_index
function build_sql_query(){
	
    
	if [[ ! -z $SECS ]];then 
	
            #validate seconds is decimal
	    is_decimal $SECS	
            if [[ $? -eq FALSE ]];then
   	        echo "seconds parameter must be decimal. aborting run!"
		error_exit $LINENO
	    fi
		
	    OP_INDEX=1
	    TIME_PART="AND TIME >= $SECS"
	    SQL_QUERY="$SQL_QUERY $TIME_PART"
	fi

	if [[ ! -z $OWNER ]];then 
	    OP_INDEX=1
	    USER_PART="AND USER = '$OWNER'"
	    SQL_QUERY="$SQL_QUERY $USER_PART"
	fi
	
	if [[ ! -z $QUERY ]];then 
	    OP_INDEX=1
	    INFO_PART="AND INFO LIKE '%$QUERY%'"
    	    SQL_QUERY="$SQL_QUERY $INFO_PART"
	fi

	if [[ $OP_INDEX -eq 0 ]];then
		echo "****** $PROGNAME - running in list mode ******"
	else
		echo "****** $PROGNAME - running in killing mode ******"
	fi
	
}


# runs the sql query and calls sql kill command on relavent threads
function process_query(){

	
	mysql -u $USER_NAME -p"$PASSWORD" --skip-column-names -e "$SQL_QUERY" | while read id user time state info ; do	
    	
    	# kill mode
	    if [[ ! $OP_INDEX -eq 0 ]]; then  

                    # kill command for thread with id $id
                    mysql -u $USER_NAME -p"$PASSWORD" --skip-column-names -e"kill $id" &> /dev/null
					
					if [[ $? -eq 0 ]];then
                        echo "### KO - ID: $id | USER $user | TIME: $time |STATE: $state | INFO: $info"
	        		fi
            # list mode
	    else # 
    	        echo "### Mysql Thread - ID: $id | USER $user | TIME: $time |STATE: $state | INFO: $info" 
	    fi
	done
	
	
	
}


#----------------------------------------------------------------------
# Script flow start - "main"
#----------------------------------------------------------------------

# get options from command line
while getopts ":u:p:s:q:o:h" opt; do
    case $opt in
        u)
	    USER_NAME=$OPTARG
            ;;	  
        p)
	    PASSWORD=$OPTARG    
            ;;
            
        s)
            SECS=$OPTARG  
            ;;
        q)
            QUERY=$OPTARG 
            ;;
        o)
            OWNER=$OPTARG 
            ;;
        h)
            usage
            ;;
       \?)
           usage
           ;;
       :)
	   usage
	   ;;	  
    esac
done

echo
#echo "$PROGNAME: looking to kill some mysql threads" 

# build the sql query according to the user input
build_sql_query

#print out user if no password is entered
if [[ -z $PASSWORD ]];then
	echo "No password was passed on for user $USER_NAME"
fi

#run query and kill relevant threads
process_query

echo 
exit 0
